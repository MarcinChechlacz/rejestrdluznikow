﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RejestrDluznikow
{
    class Dluznik
    {
        private string s_Imie;
        private string s_Nazwisko;
        private string s_Pesel;

        public string Imie
        {
            get
            {
                return s_Imie;
            }
        }

        public string Nazwisko
        {
            get
            {
                return s_Nazwisko;
            }
        }

        public string Pesel
        {
            get
            {
                return s_Pesel;
            }
        }

        public void InsertFirstName()
        {
            Console.WriteLine("Podaj imie\n");
            string imieTmp = "";
            while (imieTmp == "" || imieTmp == null)
            {
                imieTmp = Console.ReadLine();
                if(imieTmp == "")
                    Console.WriteLine("Imie nie moze byc puste");
                else if(imieTmp == null)
                    Console.WriteLine("Imie nie moze miec wartości null");
            }
            this.s_Imie = imieTmp;
        }

        public void InsertLastName()
        {
            Console.WriteLine("Podaj nazwisko\n");
            string tmp = "";
            while (tmp == "" || tmp == null)
            {
                tmp = Console.ReadLine();
                if (tmp == "")
                    Console.WriteLine("Nazwisko nie moze byc puste");
                else if (tmp == null)
                    Console.WriteLine("Nazwisko nie moze miec wartości null");
            }
            this.s_Nazwisko = tmp;
        }

        public void InsertPesel()
        {
            Console.WriteLine("Podaj PESEL");
            string tmp = "";
            bool correctPesel = false;
            while (tmp == "" || tmp == null || correctPesel == false)
            {
                tmp = Console.ReadLine();
                if (tmp == "")
                    Console.WriteLine("PESEL nie moze byc pusty");
                else if (tmp == null)
                    Console.WriteLine("PESEL nie moze miec wartości null");
                correctPesel = PeselValidation(tmp);
                if(!correctPesel)
                    Console.WriteLine("Wprowadz prawidlowy pesel");
            }
            this.s_Pesel = tmp;
        }

        private bool PeselValidation(string input)
        {
            int[] weights = { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };
            bool result = false;
            if (input.Length == 11)
            {
                int controlSum = CalculateControlSum(input, weights);
                int controlNum = controlSum % 10;
                controlNum = 10 - controlNum;
                if (controlNum == 10)
                {
                    controlNum = 0;
                }
                int lastDigit = int.Parse(input[input.Length - 1].ToString());
                result = controlNum == lastDigit;
            }
            return result;
        }

        private static int CalculateControlSum(string input, int[] weights, int offset = 0)
        {
            int controlSum = 0;
            for (int i = 0; i < input.Length - 1; i++)
            {
                controlSum += weights[i + offset] * int.Parse(input[i].ToString());
            }
            return controlSum;
        }

        public void PrintInformation()
        {
            Console.WriteLine($"{this.s_Imie}\t{this.s_Nazwisko}\t{this.s_Pesel}");
        }

    }
}
