﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace RejestrDluznikow
{
    class Program
    {
        static FileStream bazaDluznikow = new FileStream("BazaDluznikow.csv",
            FileMode.OpenOrCreate, FileAccess.ReadWrite);
        static StreamWriter bazaDluznikowSW = new StreamWriter(bazaDluznikow);

        static FileStream bazaDlugow = new FileStream("BazaDlugow.csv",
            FileMode.OpenOrCreate, FileAccess.ReadWrite);
        static StreamWriter bazaDlugowSW = new StreamWriter(bazaDlugow);

        static void Main(string[] args)
        {    
            var dluznicy = new List<Dluznik>();
            var dlugi = new List<Dlugi>();

            bool runProgram = true;
            while (runProgram)
            {
                string choice = "";
                Console.WriteLine("Co chcesz zrobic?\n1.Dodac dluznika\n2.Wyswietlic dluznikow\n3.Usunac dluznika\n4.Wyswietlic dlugi");
                Console.WriteLine("5. Dodac dlug\n6. Zapisac do pliku\n7. Wyjsc");
                choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        DodajDluznika(ref dluznicy);
                        break;
                    case "2":
                        WyswietlDluznika(dluznicy);
                        break;
                    case "3":
                        UsunDluznika(ref dluznicy);
                        break;
                    case "4":
                        WyswietlDlugi(dlugi, dluznicy);
                        break;
                    case "5":
                        DodajDlug(ref dlugi, ref dluznicy);
                        break;
                    case "6":
                        ZapiszDoPliku(dlugi,dluznicy);
                        break;
                    default:
                        runProgram = false;
                        break;
                }
            }
            bazaDlugowSW.Dispose();
            bazaDlugow.Dispose();
            bazaDluznikowSW.Dispose();
            bazaDluznikow.Dispose();
        }

        private static void ZapiszDoPliku(List<Dlugi> dlugi, List<Dluznik> dluznicy)
        {
            foreach(Dluznik person in dluznicy)
            {
                bazaDluznikowSW.WriteLine($"{person.Imie},{person.Nazwisko},{person.Pesel}"); 
            }
            foreach(Dlugi debt in dlugi)
            {
                bazaDlugowSW.WriteLine($"{debt.Kwota},{debt.Typ},{debt.Status}");
            }
        }

        private static void DodajDlug(ref List<Dlugi> dlugi, ref List<Dluznik> dluznicy)
        {
            Console.WriteLine("Podaj PESEL");
            var pesel = Console.ReadLine();
            var istniejeDluznik = false;
            foreach(Dluznik person in dluznicy)
            {
                if (person.Pesel == pesel)
                {
                    istniejeDluznik = true;
                    break;
                }
            }

            if (!istniejeDluznik)
            {
                Console.WriteLine("Podany dluznik nie istnieje w bazie\nWprowadz dluznika");
                DodajDluznika(ref dluznicy);
            }

            Dlugi newDlug = new Dlugi();
            newDlug.InsertPesel();
            newDlug.InsertStatus();
            newDlug.InsertKwota();
            newDlug.InsertType();

            dlugi.Add(newDlug);
        }

        private static void WyswietlDlugi(List<Dlugi> allDlugi,
            List<Dluznik> allDluznicy)
        {
            Console.WriteLine("Imie\tNazwisko\tPesel\tKwota\tTyp\tStatus");
            foreach(Dlugi debt in allDlugi)
            {
                foreach(Dluznik person in allDluznicy)
                {
                    if (debt.Pesel == person.Pesel)
                    {

                        Console.Write($"{person.Imie}\t{person.Nazwisko}\t{person.Pesel}\t");
                        break;
                    }
                }
                Console.Write($"{debt.Kwota}\t{debt.Typ}\t{debt.Status}\n");
            }
        }

        private static void UsunDluznika(ref List<Dluznik> dluznicy)
        {
            Console.WriteLine("Wprowadz PESEL uzytkownika ktorego chesz usunac");
            string pesel = Console.ReadLine();
            dluznicy.Remove(dluznicy.Single(s => s.Pesel == pesel));
        }

        private static void WyswietlDluznika(List <Dluznik> all)
        {
            Console.WriteLine("Imie\tNazwisko\tPESEL");
            foreach(Dluznik person in all)
            {
                person.PrintInformation();
            }
            Console.WriteLine();
        }

        private static void DodajDluznika(ref List<Dluznik> dluznicy)
        {
            Dluznik newDluznik = new Dluznik();
            newDluznik.InsertFirstName();
            newDluznik.InsertLastName();
            newDluznik.InsertPesel();
            dluznicy.Add(newDluznik);
        }
    }
}
