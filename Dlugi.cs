﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RejestrDluznikow
{
    class Dlugi
    {
        private string s_Pesel;
        private float f_Kwota;
        private string s_Status;
        private string s_Typ;

        public string Pesel
        {
            get
            {
                return s_Pesel;
            }
        }
        public float Kwota
        {
            get
            {
                return f_Kwota;
            }
        }
        public string Status
        {
            get
            {
                return s_Status;
            }
        }
        public string Typ
        {
            get
            {
                return s_Typ;
            }
        }

        public void InsertPesel()
        {
            Console.WriteLine("Podaj PESEL");
            string tmp = "";
            bool correctPesel = false;
            while (tmp == "" || tmp == null || correctPesel == false)
            {
                tmp = Console.ReadLine();
                if (tmp == "")
                    Console.WriteLine("PESEL nie moze byc pusty");
                else if (tmp == null)
                    Console.WriteLine("PESEL nie moze miec wartości null");
                correctPesel = PeselValidation(tmp);
                if (!correctPesel)
                    Console.WriteLine("Wprowadz prawidlowy pesel");
            }
            this.s_Pesel = tmp;
        }

        private bool PeselValidation(string input)
        {
            int[] weights = { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };
            bool result = false;
            if (input.Length == 11)
            {
                int controlSum = CalculateControlSum(input, weights);
                int controlNum = controlSum % 10;
                controlNum = 10 - controlNum;
                if (controlNum == 10)
                {
                    controlNum = 0;
                }
                int lastDigit = int.Parse(input[input.Length - 1].ToString());
                result = controlNum == lastDigit;
            }
            return result;
        }

        private static int CalculateControlSum(string input, int[] weights, int offset = 0)
        {
            int controlSum = 0;
            for (int i = 0; i < input.Length - 1; i++)
            {
                controlSum += weights[i + offset] * int.Parse(input[i].ToString());
            }
            return controlSum;
        }

        public void InsertKwota()
        {
            Console.WriteLine("Podaj kwote");
            bool poprawnaKwota = false;
            float kwota = 0;
            double kwota_tmp = 0;
            while (!poprawnaKwota)
            {
                try
                {
                    kwota_tmp = double.Parse(Console.ReadLine());
                    kwota = (float)kwota_tmp;
                }
                catch (Exception)
                {
                    Console.WriteLine("Kwota musi miec wartosc zmienno przecinkowa, w formacie #,##");
                    continue;
                }
                finally
                {
                    if (!(kwota > 1.0F))
                    {
                        poprawnaKwota = false;
                        Console.WriteLine("Kwota musi byc wieksza niz 0");
                    }
                    else
                        poprawnaKwota = true;

                }
            }

            this.f_Kwota = kwota;
        }

        public void InsertType()
        {
            Console.WriteLine("Podaj typ dlugu");
            string tmp = "";
            while (tmp == "" || tmp == null)
            {
                tmp = Console.ReadLine();
                if (tmp == "")
                    Console.WriteLine("Typ nie moze byc pusty");
                else if (tmp == null)
                    Console.WriteLine("Typ nie moze miec wartości null");
            }
            this.s_Typ = tmp;
        }
        public void InsertStatus()
        {
            Console.WriteLine("Podaj status dlugu");
            string tmp = "";
            while (tmp == "" || tmp == null)
            {
                tmp = Console.ReadLine();
                if (tmp == "")
                    Console.WriteLine("Status nie moze byc pusty");
                else if (tmp == null)
                    Console.WriteLine("Status nie moze miec wartości null");
            }
            this.s_Status = tmp;
        }

        public void PrintInformation()
        {
            Console.WriteLine($"{this.f_Kwota}\t{this.s_Typ}\t{this.s_Status}");
        }
    }
}
